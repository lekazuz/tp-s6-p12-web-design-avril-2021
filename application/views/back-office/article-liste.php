<div class="row">
    <div class="col-md-5 mx-auto">
        <ul class="list-group">
            <?php foreach ($articles as $article) { ?>
            <li class="list-group-item mb-0 d-flex justify-content-between align-items-center">
            <div class="">
                <h5><?php echo $article->getTitre(); ?></h5>
                <p><?php echo $article->getSubContenue(200); ?></p>
            </div>
            <a href="<?php echo site_url(); ?>back-office/articles/maj/<?php echo $article->getIdArticle(); ?>"><i class="fas fa-edit"></i></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>