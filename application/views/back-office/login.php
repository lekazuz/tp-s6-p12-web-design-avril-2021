<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="login-admin" />
        <meta name="author" content="Randriamarosaina Sombiniaina Fitahiana" />
        <title>Login - Admin</title>
        <link href="<?php echo site_url(); ?>/assets/css/styles.css" rel="stylesheet" />
        <script src="<?php echo site_url(); ?>/assets/fontawesome-5.15.2/js/all.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-primary">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header"><h3 class="text-center font-weight-light my-4">Admin - Login</h3></div>
                                    <div class="card-body">
                                        <form action="<?php echo site_url(); ?>back-office/admin/login" method="post">
                                            <div class="form-group">
                                                <label class="small mb-1" for="inputPseudo">Pseudo</label>
                                                <input name="pseudo" class="form-control py-4" id="inputinputPseudo" type="text" placeholder="Entrer votre pseudo" />
                                            </div>
                                            <div class="form-group">
                                                <label class="small mb-1" for="mdp">Mot de passe</label>
                                                <input name="mdp" class="form-control py-4" id="mdp" type="password" placeholder="Entrer votre mot de passe" />
                                            </div>
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                                <button role="submit" class="btn btn-primary">Login</button>
                                            </div>
                                        </form>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2021</div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="<?php echo site_url(); ?>/assets/Bootstrap/Bootstrap/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo site_url(); ?>/assets/Bootstrap/Bootstrap/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo site_url(); ?>/assets/js/scripts.js"></script>
    </body>
</html>