<div class="row">
    <div class="col-md-5 mx-auto">
        <form action="<?php echo site_url(); ?>back-office/articles/<?php echo (isset($article)) ? "update" : "save" ?>" method="post">
            <div class="form-group">
                <label for="titre">Titre :</label>
                <input name="titre" type="text" class="form-control" id="titre" placeholder="le Titre de l'article" value="<?php if (isset($article)) echo $article->getTitre(); ?>">
            </div>
            <div class="form-group">
                <label for="contenue">Contenue : </label>
                <textarea name="contenue" class="form-control" id="contenue" placeholder="le contenue de l' article" rows="5"><?php if (isset($article)) echo $article->getContenue(); ?></textarea>
            </div>
            <div class="form-group">
                <label for="motsCle">Les Mots Cle : </label>
                <input name="motsCle" type="text" class="form-control" id="motsCle" placeholder="les mots cle" aria-describedby="motsCleHelp" value="<?php if (isset($article)) echo $article->getMotscle(); ?>">
                <small id="motsCleHelp" class="form-text text-muted">Separez les mots cle par un virgule " , "</small>
            </div>
            <div class="form-group">
                <label for="date">Date :</label>
                <input name="date" type="date" class="form-control" id="date" placeholder="la Date de l'article" value ="<?php if (isset($article)) echo date("Y-m-d",  $article->getDate()); ?>">
            </div>
            <?php if (isset($article)) {?>
                <div class="form-group">
                <input name="idArticle" type="hidden" class="form-control" value ="<?php echo $article->getIdArticle(); ?>">
            </div>
            <?php } ?>
            <button name="save" type="submit" class="btn btn-primary">Publier</button>
        </form>
    </div>
</div>