<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function index(){
        if(isset($_SESSION['log']))
            redirect('back-office/articles', 'refresh');
        $this->load->view('back-office/login');
    }

    public function login(){
        $pseudo = $this->input->post('pseudo');
        $mdp = $this->input->post('mdp');
        if($pseudo == 'admin' && $mdp == '753admin'){
            $_SESSION['log'] = 'admineb ';
            redirect('/back-office/articles', 'refresh');
        }
        else{
            $this->index();
        }
    }

    public function logout(){
        unset($_SESSION['log']);
        $this->index();
    }
}

?>          
