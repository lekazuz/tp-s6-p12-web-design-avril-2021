    sudo mysql -u root -p
123456

CREATE DATABASE info_covid;
CREATE USER 'admin_info_covid'@'localhost' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON info_covid.* TO 'admin_info_covid'@'localhost';

mysql -u admin_info_covid info_covid -p

CREATE TABLE ARTICLE(
    IDARTICLE INT NOT NULL AUTO_INCREMENT,
    TITRE TEXT NOT NULL,
    CONTENUE TEXT NOT NULL,
    DATEARTICLE DATE NOT NULL,
    MOTSCLE TEXT NOT NULL,
    PRIMARY KEY (IDARTICLE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;